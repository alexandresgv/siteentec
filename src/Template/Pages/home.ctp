<?= $this->assign('title', 'home'); ?>

		<div style="padding-top: 50px;"></div>
		<div style="opacity: 1; " id="home">
        <img src="img/banner-site-ii-stoli_red_comp.png" alt="">
        <div class="container">
            <div class="row">
            </div>
        </div>
    </div>

    <div id="sobre" class="section">
      <h1 class="text-center">Sobre o Evento</h1>
      <div class="container">
        <div class="row">
          <div class="col-md-10 col-md-offset-1 text-justify">
            
            <p>
              Em um cenário mundial globalizado e de crescentes mudanças a mobilidade urbana se  apresenta como um desafio frente às políticas ambientais. O desenvolvimento  econômico e social do Brasil possibilitou um aumento significativo no padrão de  mobilidade concentrado no transporte motorizado individual, que vem mostrando-se  insustentável, tanto no que concerne as necessidades de deslocamento caracterizada pela  vida urbana, quanto ao que tange a proteção ambiental.
            </p>
            <p>
              A logística se apresenta como um elo, para que a mobilidade urbana e o  desenvolvimento sustentável se complementem e orientem a ocupação e utilização das  cidades para promover a melhor forma de acesso às regiões urbanas, minimizando os  danos causados ao ambiente que habitamos. Da mesma forma, na estrutura,  possibilidade de acesso e disponibilidade da infraestrutura urbana, tais como as redes de  transporte público que proporcionam condições de mobilidade para indivíduos de forma  isolada, bem como para comunidades inteiras.
              </p>
              <p>
              Desafios referentes ao transporte urbano de cargas contribuem ainda significativamente  dentro deste cenário, visto que desempenham um papel fundamental na manutenção das  atividades comerciais, industriais e de serviços, contribuindo para a competividade do  mercado. No entanto impactam diretamente no desempenho econômico da região,  trazendo sérias consequências ao meio ambiente relacionado ao consumo de energia,  poluição ambiental, sonora e visual, dentre outros.
              </p>
              <p>
              Diante da necessidade de discutir as possibilidades e mudanças nos padrões mais  convencionais e tradicionais de mobilidade, dentro de uma perspectiva de cidades mais  sustentáveis e justas, surge o II Simpósio Tecnológico de Operações e Logística do  IFPE, com a temática “Logística urbana e sustentabilidade” que tratará de propostas  relacionadas à Inovação, logística reversa, sustentabilidade, mobilidade urbana e  qualidade, buscando soluções adequadas para discussões de políticas públicas e  demandas de mercado da região.
               </p>
              <p>Contamos com sua participação
              <br>
              Josefa Renata Queiroz da Costa Gomes
              <br>
              Comitê de Organização
              </p>
           
          </div>
        </div>
      </div>
    </div>




    <div id="attractions" class="section">
      <h1 class="text-center">Participações</h1>
      <div class="container">
        <div class="row">
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="thumbnail attraction">
                  <img src="img/stoli/Danilo.jpg" alt="" class="img-rounded img-responsive">
                  <div class="caption">
                    <h4>
                      Prof. Danilo Soares<br>
                      <small>SENAI Areias</small>
                    </h4>
                    <h5 class="text-center">
                      <a href="#" data-toggle="modal" data-target="#modal_danilo"><span class="fa fa-plus-square"></span> Saiba mais</a>
                    </h5>
                    <div class="modal fade" id="modal_danilo" tabindex="-1" role="dialog" aria-labelledby="pablo_title">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="pablo_title">Prof. Danilo Soares</h4>
                          </div>
                          <div class="modal-body">
                            <p>
                              Graduado em Administração e  Especialista em Educação Profissional e Tecnológica pelo SENAI SETIQT. Atua em consultorias empresariais na área de gestão e negócios. Professor efetivo do SENAI Areias nas áreas de Gestão, produção e logística.
                            </p>
                            <p>
                              Palestra: <strong>Mobilidade Urbana</strong>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-10 col-md-offset-1"> 
                <div class="thumbnail attraction">
                  <img src="img/stoli/FOTO_Edilene.jpg" alt="" class="img-rounded img-responsive">
                  <div class="caption">
                    <h4>
                      Profª Edilene Félix<br>
                      <small>IFPE - Igarassu</small>
                    </h4>
                    <h5 class="text-center">
                      <a href="#" data-toggle="modal" data-target="#modal_edilene"><span class="fa fa-plus-square"></span> Saiba mais</a>
                    </h5>
                    <div class="modal fade" id="modal_edilene" tabindex="-1" role="dialog" aria-labelledby="pablo_title">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="pablo_title">Profª Edilene Félix</h4>
                          </div>
                          <div class="modal-body">
                            <p>
                              Graduada em Administração pela UPE e Mestra em Engenharia de Produção pela UFPE. Possui experiência nas áreas de educação à distância (EAD), educação de jovens e adultos e gerenciamento de projetos. Atuou em projetos como o ProJovem Urbano e Pronatec. Possui interesse de pesquisa nas áreas de educação, aprendizagem organizacional e gestão de projetos. Atualmente é professora efetiva do Instituto Federal de Pernambuco vinculada a área de Gestão e Logística.
                            </p>
                            <p>
                              Minicurso: <strong>Gestão de projetos</strong> - Voltado para profissionais e estudantes que desejam ter o primeiro contato com a Gestão de Projetos, o curso pretende apresentar os fundamentos e conceitos básicos de gerenciamento de projetos de acordo com as melhores práticas do Project Management Institute - PMI®.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>              
            </div>
          </div>
          <div class="col-md-3">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="thumbnail attraction">
                  <img src="img/stoli/Tito.jpg" alt="" class="img-rounded img-responsive">
                  <div class="caption">
                    <h4>
                      Tito Sales<br>
                      <small>Portal Archimedes</small>
                    </h4>
                    <h5 class="text-center">
                      <a href="#" data-toggle="modal" data-target="#modal_tito"><span class="fa fa-plus-square"></span> Saiba mais</a>
                    </h5>
                    <div class="modal fade" id="modal_tito" tabindex="-1" role="dialog" aria-labelledby="pablo_title">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="pablo_title">Prof. Danilo Soares</h4>
                          </div>
                          <div class="modal-body">
                            <p>
                              Diretor Administrativo na empresa Portal Archimedes Negócios Digitais LTDA. Palestrante e instrutor de cursos nas áreas de gestão de projetos, gestão de pessoas e gestão estratégica. Certificado como PMP (Project Management Professional) e CAPM (Certified Associate in Project Management), pelo PMI. Bacharel em Administração de Empresas pela FCAP/UPE. Especialista em Tecnologia da Informação e Comunicação na Educação. Coordenador de tecnologia do Instituto ADM&amp;TEC.
                            </p>
                            <p>
                              Palestra: <strong>Desafios logísticos em grandes projetos estruturadores na Região Metropolitana de Recife</strong>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="thumbnail attraction">
                  <img src="img/stoli/lucio_ribeiro_280x280.jpg" alt="" class="img-rounded img-responsive">
                  <div class="caption">
                    <h4>
                      Lúcio Ribeiro<br>
                      <small>DUIT</small>
                    </h4>
                    <h5 class="text-center">
                      <a href="#" data-toggle="modal" data-target="#modal_lucio"><span class="fa fa-plus-square"></span> Saiba mais</a>
                    </h5>
                    <div class="modal fade" id="modal_lucio" tabindex="-1" role="dialog" aria-labelledby="pablo_title">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="pablo_title">Lúcio Ribeiro</h4>
                          </div>
                          <div class="modal-body">
                            <p>
                              Lúcio trabalha com tecnologia da informação há 15 anos. Cursou engenharia da computação - UFPE, empreendedor, fundou 3 empresas e passou por programas de incubação e aceleração no Porto Digital, CESAR e ITEP.
                            </p>
                            <p>
                              Palestra:  <strong>Inovação tecnológica e sustentabilidade</strong>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="col-md-3">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="thumbnail attraction">
                  <img src="img/stoli/tarcisio_280_280.png" alt="" class="img-rounded img-responsive">
                  <div class="caption">
                    <h4>
                      Prof. José Tarcísio<br>
                      <small>IFPE - Igarassu</small>
                    </h4>
                    <h5 class="text-center">
                      <a href="#" data-toggle="modal" data-target="#modal_tarciso"><span class="fa fa-plus-square"></span> Saiba mais</a>
                    </h5>
                    <div class="modal fade" id="modal_tarciso" tabindex="-1" role="dialog" aria-labelledby="tarcisio_title">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="tarcisio_title">José Tarcísio</h4>
                          </div>
                          <div class="modal-body">
                            <p>
                              Possui graduação em administração de empresas pela Universidade de Pernambuco e Especialização em Logística empresarial pela Universidade de Pernambuco. Com sólida experiência nas áreas de logística de suprimentos,produção, armazenagem, transporte e distribuição. Atuando nas atividades de Consultoria, gestão dos processos de planejamento de suprimentos e compras, planejamento, programação e controle da produção e gestão da armazenagem e distribuição, em empresas multinacionais e nacionais de grande porte . Atuou como Professor do Curso técnico em Logística no SENAC - Serviço Nacional de Aprendizagem Industrial e do SENAI - Serviço Nacional de Aprendizagem Industrial. Atualmente é professor efetivo do ensino básico, técnico e tecnológico, na área de Gestão e Logística no Instituto Federal de Educação de Pernambuco Campus Igarassu.
                            </p>
                            <p>
                              Minicurso:  <strong>Estratégias e Planejamento de Produção</strong>                              
                            </p>
                            <p>
                              Conteúdo a ser abordado : Estratégias de produção. Planejamento de vendas e operações. Planejamento - mestre da produção. MRP Planejamento de necessidades materiais. MRP II Planejamento dos recursos de manufatura.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="col-md-3">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="thumbnail attraction">
                  <img src="img/stoli/jose_mario_280_280.jpg" alt="" class="img-rounded img-responsive">
                  <div class="caption">
                    <h4>
                      Prof. José Mário<br>
                      <small>IFPE - Cabo de Santo Agostinho</small>
                    </h4>
                    <h5 class="text-center">
                      <a href="#" data-toggle="modal" data-target="#modal_mario"><span class="fa fa-plus-square"></span> Saiba mais</a>
                    </h5>
                    <div class="modal fade" id="modal_mario" tabindex="-1" role="dialog" aria-labelledby="mario_title">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="mario_title">José Mário</h4>
                          </div>
                          <div class="modal-body">
                            <p>
                               Docente do Instituto Federal de Pernambuco - Campus Cabo de Santo Agostinho. Pesquisador e estudioso na área de Logística Reversa e Economia Circular. Mestre em Administração e Desenvolvimento Rural pela Universidade Federal Rural de Pernambuco - UFRPE.  Com MBA em Logista Empresarial pela Faculdade de Ciência da Administração de Pernambuco - FCAP. Graduado em Administração de Empresas pela Faculdade Boa Viagem - FBV. Atuando na área acadêmica desde de 2009 e na área logística desde de 2003 desenvolvendo projetos em empresas nacionais de grande porte nas seguintes atividades: elaboração e treinamento de normas de procedimento, administração de frota, análise e pagamento de processos de avarias e extravios, implantação de sistema de logística, transporte e WMS, implantação do Programa 5S e coleta seletiva, sistema de rastreamento de veículos e distribuição.
                            </p>
                            <p>
                              Minucurso: <strong>Logística Reversa & Economia Circular</strong>
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          




        </div>
      </div>
    </div>








    <div class="section" id="program">
      <h1 class="text-center">Programação</h1>
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h3 class="text-success text-uppercase">Quinta-feira, 6 de Outubro</h3>
          </div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>08:30h às 09:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Credenciamento
              <span class="where">(Hall)</span>
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>09:30h às 10:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Solenidade de Abertura
              <span class="where">(Auditório) </span>
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>10:30h às 12:00h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Palestra com Ivan Carlos Cunha (Diretor de Consultoria e Engenharia na empresa <a href="https://www.facebook.com/TCE-Gest%C3%A3o-e-Mobilidade-275441542651649/?ref=br_rs" target="_blank">TCE Gestão e Mobilidade</a>)
              <span class="where">(Auditório) </span>
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>12:00h às 13:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Intervalo para almoço
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>13:00h às 14:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>Apresentação de Trabalhos
              <span class="where"></span></h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>13:30h às 16:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-3">
                <h5>
                  Minicurso: <strong>Mobilidade urbana</strong> - <a href="#" data-toggle="modal" data-target="#modal_danilo">Danilo Soares</a>
                  <span class="where"></span>
                </h5>
              </div>
              <div class="col-md-3">
                 <h5>
                  Minucurso: <strong>Logística Reversa & economia circular</strong> - José Mário (IFPE Campus Cabo de Santo Agostinho)
                  <span class="where"></span>
                </h5>
              </div>
              <div class="col-md-3">
                <h5>
                  Minicurso: <strong>Jogos Logísticos</strong> - Mariana Pereira
                  <span class="where"></span>
                </h5>
              </div>
              <div class="col-md-3">
                <h5>
                  Minicurso: <strong>Estratégia e planejamento de produção</strong> - José Tarcísio - (IFPE Campus Igarassu)
                  <span class="where"></span>
                </h5>
              </div>
              
            </div>
          </div>
          <div class="col-md-12"><hr></div>
        </div>
        <div class="row">
          <div class="col-md-2">
            <h5><strong>15:30h às 16:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Palestra: <strong>Inovação tecnológica e sustentabilidade</strong> - Lúcio Ribeiro (Empresa DUIT)
              <span class="where">(Auditório)</span>
            </h5>
          </div>
        </div>
      </div>

      <div class="container">
        <hr>
        <div class="row">
          <div class="col-md-12">
            <h3 class="text-success text-uppercase">Sexta-feira, 7 de Outubro</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2">
            <h5><strong>09:00h às 10:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Mesa redonda: <strong>Resíduos Sólidos e Sustentabilidade Criativa </strong> - Srª Ilka Djanira (Secretaria de Meio Ambiente e Biodiversidade de Igarassu) 
              <span class="where">(Auditório)</span>
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>10:30h às 12:00h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Mesa redonda: <strong>Transportes ativos na mobilidade urbana</strong> - Empresa Ameciclo
              <span class="where">(Auditório)</span>
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>12:00h às 13:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Intervalo para almoço
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>13:30h às 16:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <div class="row">
              <div class="col-md-4">
                <h5>
                  Minicurso: <strong>Gestão de projetos</strong> - <a href="#" data-toggle="modal" data-target="#modal_edilene">Edilene Fêlix (IFPE)</a>
                  <span class="where"></span>
                </h5>
              </div>
              <div class="col-md-4">
                 <h5>
                  Minicurso: <strong>Pesquisa Operacional Aplicada à Logística</strong> - Demetrios Valença
                  <span class="where"></span>
                </h5>
              </div>
              <div class="col-md-4">
                 <h5>
                  Minicurso: <strong>Planejamento e Programação da Produção</strong> - Érica Moreira (UFRPE)
                  <span class="where"></span>
                </h5>
              </div>
            </div>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2"><h5><strong>14:00h às 17:00h</strong></h5></div>
          <div class="col-md-10">
            <h5>
              Minicurso: <strong>Gerenciamento de processos de negócios com o Bizagi</strong> - Guilherme Amorim (IFPE Campus Ipojuca)
              <span class="where"></span>
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>14:30h às 15:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Palestra: <strong>Desafios logísticos em grandes projetos estruturadores na Região Metropolitana de Recife </strong> - <a href="#" data-toggle="modal" data-target="#modal_tito">Tito Sales</a>
              <span class="where">(Auditório)</span>
            </h5>
          </div>
          <div class="col-md-12"><hr></div>
        </div>

        <div class="row">
          <div class="col-md-2">
            <h5><strong>16:00h às 16:30h</strong></h5>
          </div>
          <div class="col-md-10">
            <h5>
              Solenidade de encerramento
              <span class="where">(Auditório)</span>
            </h5>
          </div>
        </div>
      </div>







    <div id="academic" class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-justify">
            <h2 class="text-center">Mostra de Trabalho de Pesquisa e Extensão em Tecnologia</h2>
            <p>
              A <strong>Mostra de Pôsteres</strong> tem como objetivo a apresentação de trabalho científico, relato de projetos ou estudos em andamento.
            </p>

            <p>
              Antes de inscrever um trabalho é necessário que o interessado leia atentamente as regras para submissão de pôsteres.
            </p>

            <p>
              Os critérios descritos nas regras de submissão de pôsteres devem ser totalmente respeitados para que o pôster seja aceito para avaliação.
            </p>

            <h4>Regras para submissão de Pôsteres </h4>

            <p>
              O interessado em apresentar pôster poderá inscrever apenas um único trabalho.
            </p>

            <p>
              Cada pôster pode ter 1(um) autor e até 4 (quatro) coautores.
            </p>

            <p>
              Os pôsteres poderão ser submetidos em uma das 8 áreas temáticas, a saber:
              <ul>
                <li>Gestão de operações</li>
                <li>Gestão da qualidade</li>
                <li>Logística e Cadeia de Suprimentos</li>
                <li>Ergonomia e segurança do trabalho</li>
                <li>Estratégia de operações e competitividade</li>
                <li>Gestão do conhecimento, inovação e tecnologia</li>
                <li>Gestão ambiental, sustentabilidade e logística reversa</li>
                <li>Educação em Operações e Logística</li>
              </ul>
            </p>

            <p>
              Para submeter um pôster, será preciso o proponente enviar o pôster, no formato disponibilizado, para o seguinte endereço de e-mail: <strong>poster.stoli@igarassu.ifpe.edu.br</strong>. Também será necessário preencher o formulário de inscrição e enviar ambos em anexo.
            </p>

            <p>
              Clique aqui para acessar o <a target="_blank" href="https://drive.google.com/file/d/0ByMFPgEsRHS8akprN1l5M2FpbVk/view?usp=sharing">formulário de inscrição</a> e o <a target="_blank" href="https://drive.google.com/file/d/0ByMFPgEsRHS8Qm9acDRXRENXS2M/view?usp=sharing">modelo do pôster</a>.
            </p>

            <p>
              As inscrições para submissão dos pôsteres estarão abertas de 14 a 29 de Setembro de 2016 (até às 23h59).
            </p>

            <p>
              No pôster deverá ser informado:
              <ul>
                <li>No cabeçalho: a área temática, o título do trabalho, a instituição, o(s) nome(s) completo, seguido do e–mail, do autor e coautores.</li>
                <li>No corpo do pôster: introdução, resultados, metodologia, conclusão e bibliografia.</li>
              </ul>
            </p>

            <p>
              O título das seções deve estar com tamanho 60 e do conteúdo com 32. Todo o pôster deve estar com fonte arial. As figuras e tabelas não devem ocupar mais de 50% (cinquenta por cento) do banner, devendo ser indicado a fonte das mesmas.
            </p>

            <p>
              A confirmação da inscrição do pôster será dada num prazo de 72 horas, por meio do e-mail do remetente da inscrição. Caso não receba o e-mail de confirmação, entrar em contato com a organização do evento através do e-mail: contato.stoli@igarassu.ifpe.edu.br.
            </p>


            <h4>Requisitos de Avaliação</h4>

            <p>
              O pôster inscrito será submetido à avaliação por uma banca examinadora, coordenada pelo comitê científico.
            </p>

            <p>
              Os critérios de avaliação serão:
              <ul>
                <li>Atendimento à formatação informada e ao modelo disponibilizado;</li>
                <li>Clareza, objetividade e correta redação;</li>
                <li>Relevância do conteúdo para a área temática.</li>
              </ul>
            </p>

            <p>
              O parecer de avaliação será apenas aprovado ou não aprovado.
            </p>

            <p>
              A lista dos trabalhos aprovados será divulgada no dia 30 de Setembro de 2016.
            </p>


            <h4>Regras para apresentação de Pôsteres</h4>

            <p>
              Os pôsteres serão exibidos no primeiro dia do evento, dia 06 de Outubro. A exibição ocorrerá das 13:00h às 14:30h.
            </p>
            <p>
              Cada autor ou coautor que teve seu pôster aprovado deverá credenciar seu pôster 30 minutos antes do horário da exibição para localização de seu espaço de exposição. Só receberá certificado os autores ou coautores que apresentar o pôster no dia e horário informado no resultado da avaliação.
            </p>
            <p>
              No certificado constará o nome do autor e coautores e o título do trabalho.
            </p>
            <p>
              O local de exposição do pôster estará numerado, sendo informado ao autor ou coautor no dia do evento, quando do credenciamento.
            </p>
            <p>
              Os pôsteres podem ser confeccionados em qualquer tipo de material, devendo obedecer a medida de 90cm x 120 cm, em formato retrato.
            </p>
            <p>
              A confecção e o transporte do pôster são de responsabilidade do(s) autor(es).
            </p>
            <p>
              Só serão aceitos para apresentação os pôsteres que seguirem o modelo disponibilizado neste site.
            </p>
            <p>
              Os casos omissos serão resolvidos pela comissão científica do evento.
            </p>

            
            <!-- <blockquote>
              <p>
                Os resumos devem ser enviados para o e-mail <strong>contatoentec@igarassu.ifpe.edu.br</strong> com o título “[Mostra de Pesquisa e Extensão] TÍTULO_DO_TRABALHO” até o dia <strong>01 de Abril de 2016</strong>.
              </p>

              <ul>
                <li>Modelo de Resumo:
                  <a target="blank" href="https://drive.google.com/file/d/0B7jr_kAKet92aEctdXJON0ExLVk">.doc</a>
                  <a target="blank" href="https://drive.google.com/file/d/0B7jr_kAKet92Z1pGc0lybzZneTA">.docx</a>
                  <a target="blank" href="https://drive.google.com/file/d/0B7jr_kAKet92Y0NScFRzNnczZ2M">.pdf</a>
                </li>
                <li>Modelo de Apresentação:
                    <a target="blank" href="https://drive.google.com/open?id=0B7jr_kAKet92eTRzY0ctSGt5VGc">.ppt</a>
                    <a target="blank" href="https://drive.google.com/open?id=0B7jr_kAKet92NGJYVWR2OWI3am8">.pptx</a>
                </li>
              </ul>

              <p>
                Datas Importantes
                <ul>
                  <li><strong>01 de Abril de 2016</strong> - Prazo Final para a Submissão</li>
                  <li><strong>04 de Abril de 2016</strong> - Divulgação dos Projetos Aprovados</li>
                  <li><strong>06 e 07 de Abril de 2016</strong> - Mostra de Pesquisa e Extensão</li>
                </ul>
              </p>
            </blockquote> -->
          </div>
        </div>
        <h3 class="text-warning text-uppercase text-center">RESULTADO: Pôsteres Aprovados</h3> 
        <div class="row">
          <div class="col-md-8 col-md-offset-2 text-justify">
             
            Clique aqui para acessar a <a target="_blank" href="https://drive.google.com/file/d/0ByMFPgEsRHS8WV9OckdMX0pCZFk/view?usp=sharing">relação dos pôsteres aprovados</a> com os horários das apresentações.
            
          </div>
        </div>
      </div>
    </div>
    <div class="section" id="where">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2715.191062021682!2d-34.911013006151535!3d-7.855608940417701!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x77802d54bdb66e80!2sFACIG!5e0!3m2!1spt-BR!2sbr!4v1454417639340" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <div class="col-md-6">
            <h1>Local</h1>
            <h3>Instituto Federal de Pernambuco</h3>
            <h4>Campus Igarassu</h4>
            <p>
              <br>Sede Provisória Faculdade de Igarassu (Facig) – Avenida Alfredo Bandeira de Melo S/N, BR-101 Norte, Km 44, Igarassu-PE
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="section" id="people">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center text-primary">Organização</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-2">
            <img src="img/user_placeholder.png" class="center-block img-circle img-responsive">
          </div>
          <div class="col-md-4">
            <h3 class="text-left">John Doe</h3>
            <p class="text-left">Developer</p>
          </div>
        </div>
      </div>
    </div> -->








    <div id="support" class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="text-center">Apoio</h1>
          </div>
        </div>
        <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="row">
              <div class="col-md-3">
                <!-- <img src="img/" alt="" class="img-rounded img-responsive"> -->
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-6 col-md-offset-3">
                    <img src="img/facig.jpg" alt="" class="img-rounded img-responsive">
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <!-- <img src="img/" alt="" class="img-rounded img-responsive"> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>