DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` enum('admin','participante','supervisor','palestrante','voluntario') NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(11) NOT NULL,
  `nascimento` date NOT NULL,
  `sexo` enum('Masculino','Feminino') NOT NULL,
  `cep` varchar(10) NOT NULL,
  `estado` enum('Acre','Alagoas','Amapá','Amazonas','Bahia','Ceará','Distrito Federal','Espírito Santo','Goiás','Maranhão','Mato Grosso','Mato Grosso do Sul','Minas Gerais','Pará','Paraíba','Paraná','Pernambuco','Piauí','Rio de Janeiro','Rio Grande do Norte','Rio Grande do Sul','Rondônia','Roraima','Santa Catarina','São Paulo','Sergipe','Tocantins') NOT NULL,
  `cidade` varchar(35) NOT NULL,
  `bairro` varchar(45) NOT NULL,
  `logradouro` varchar(80) NOT NULL,
  `numero` varchar(8) NOT NULL,
  `complemento` varchar(8) NOT NULL,
  `instituicao` varchar(70) NOT NULL,
  `instrucao` enum('Fundamental Incompleto','Fundamental Completo','Médio Incompleto','Médio Completo','Técnico Incompleto','Técnico Completo','Superior Incompleto','Superior Completo','Mestrado Incompleto','Mestrado Completo','Doutorado Incompleto','Doutorado Completo') NOT NULL,
  `activation_code` varchar(255) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `credenciado` tinyint(1) NOT NULL DEFAULT '0',
  `imp_certificado` tinyint(1) NOT NULL DEFAULT '0',
  `rec_certificado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `atividades`;
CREATE TABLE `atividades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` enum('Palestra','Minicurso','Oficina') NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `descricao` varchar(5000) NOT NULL,
  `id_palestrante` int(10) NOT NULL,
  `horario` datetime NOT NULL,
  `local` varchar(45) NOT NULL,
  `horariofim` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_atividade_palestrante_id_idx` (`id_palestrante`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `minicursos`;
CREATE TABLE `minicursos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(150) DEFAULT NULL,
  `nome_palestrante` varchar(90) DEFAULT NULL,
  `numero_vagas` int(3) DEFAULT NULL,
  `descricao` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `palestrantes`;
CREATE TABLE `palestrantes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `perfil` varchar(4000) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `ocupacao` varchar(100) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_palestrantes_user_id_idx` (`user_id`),
  CONSTRAINT `fk_palestrantes_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--

DROP TABLE IF EXISTS `userminicursos`;
CREATE TABLE `userminicursos` (
  `user_id` int(9) NOT NULL,
  `minicurso_id` int(9) NOT NULL,
  `created` datetime NOT NULL,
  `rec_certificado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`,`minicurso_id`),
  KEY `fk_userminicursos_user_id_idx` (`user_id`),
  KEY `fk_userminicursos_minicurso_id_idx` (`minicurso_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
